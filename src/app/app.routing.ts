import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { HomeComponent } from 'src/app/library/shared/component/home/home.component';
import { AuthGuard } from './auth.guard';
import { SecurityGuard } from './security.guard';
import { UserGuard } from './user.guard';


const routes: Routes = [

    {
        path: '',
        component: HomeComponent,canActivate: [SecurityGuard]
    },
    {
        path: 'auth',
        loadChildren: './library/auth/auth.module#AuthModule',canActivate: [SecurityGuard]
    },
    {
        path: "dash-view",
        loadChildren: "./layout/layout.module#LayoutModule" ,canActivate: [AuthGuard]
        
    },
    {
        path:"adddetail",
        loadChildren:"./library/shared/shared.module#SharedModule"
    },
    {
        path:"users",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 
    },
    {
        path:"shirt-view",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    },
    {
        path:"jeans-view",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    },
    {
        path:"jeans-detail",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 


    },
    {
        path:"blog-view",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    },
    {
        path:"blog-detail",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 
    },
    {
        path:"shirt-detail",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    },
    {
        path:"addtocart",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    },
    {
        path:"shipping",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    },
    {
        path:"payment",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    },
    {
        path:"sucessfull",
        loadChildren:"./user-view/user-view.module#UserViewModule",canActivate: [UserGuard] 

    }
   
];



@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRouting {}
