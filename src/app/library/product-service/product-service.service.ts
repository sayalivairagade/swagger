import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {
 

  constructor(private http: HttpClient, myRoute: Router) { }
  url=environment.url;

  addProductData(data: any): Observable<any> {
    console.log('data', data);
    return this.http.post(this.url + 'addProductData', data);
}
  listProduct():Observable<any>{
    return this.http.get(this.url + 'listProduct');
}
  listJeans():Observable<any>{
    return this.http.get(this.url + 'listJeans');
}
  deleteProduct(id:any):Observable<any>{
    console.log(id);
    return this.http.delete(this.url +'deleteProduct?id='+ id);
}
  editProduct(id:any,data:any):Observable<any>{
      console.log(id,data);
      return this.http.put(this.url +'editProduct?id='+ id,data);
}
  
  getProductDetail(id:any):Observable<any>{
        console.log(id);
        return this.http.get(this.url + 'getProductDetail?id='+ id);
        } 
}
