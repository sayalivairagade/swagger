import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  addProductData: any;
  constructor(private http: HttpClient, myRoute: Router) { }
  url=environment.url;

  signup(data: any): Observable<any> {
    console.log('data', data);
    return this.http.post(this.url+ 'signupdata/' , data);
  }
  login(data: any): Observable<any> {
    console.log('data', data);
    return this.http.post(this.url + 'logindata', data);
  }
  addUser(data: any): Observable<any> {
    console.log('data', data);
    return this.http.post(this.url +'addUser', data);
  }
  listuser(): Observable<any> {
    return this.http.get( this.url + 'listuser');
  }
  deleteUser(id: any): Observable<any> {
    console.log(id);
    return this.http.delete(this.url +'deleteUser?id=' + id);
  }
  editUser(id: any, data: any): Observable<any> {
    console.log(id, data);
    return this.http.put(this.url + 'editUser?id=' + id, data);
  }

  getUserDetail(id: any): Observable<any> {
    console.log(id);
    return this.http.get(this.url + 'getUserDetail?id=' + id);
  }
  editUserProfile(id: any, data: any): Observable<any> {
    console.log(id, data);
    return this.http.put(this.url + 'editUserProfile?id=' + id, data);
  }

  oldPassword(id:any ,data: any): Observable<any> {
    console.log(id, data);
    return this.http.post(this.url +'oldPassword?id=' + id, data);
  }
  
  newPasswordChange(id: any, data: any): Observable<any> {
    console.log(id, data);
    return this.http.put(this.url + 'newPasswordChange?id=' + id, data);
  }

  getToken() {

    return localStorage.getItem("token")

  }

  isLoggednIn() {

    return this.getToken() !== null;

  }
}
