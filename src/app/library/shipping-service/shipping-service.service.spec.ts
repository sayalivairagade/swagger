import { TestBed, inject } from '@angular/core/testing';

import { ShippingServiceService } from './shipping-service.service';

describe('ShippingServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShippingServiceService]
    });
  });

  it('should be created', inject([ShippingServiceService], (service: ShippingServiceService) => {
    expect(service).toBeTruthy();
  }));
});
