import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ShippingServiceService {

  constructor(private http: HttpClient, myRoute: Router) { }
  url=environment.url;

  shippingDetail(data: any): Observable<any> {
    console.log('data', data);
    return this.http.post(this.url + 'shippingDetail?id=',data);
  }
}
