import { TestBed, inject } from '@angular/core/testing';

import { AddtocartServiceService } from './addtocart-service.service';

describe('AddtocartServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddtocartServiceService]
    });
  });

  it('should be created', inject([AddtocartServiceService], (service: AddtocartServiceService) => {
    expect(service).toBeTruthy();
  }));
});
