import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GlobalHeaderComponent } from './component/global-header/global-header.component';
import { GlobalFooterComponent } from './component/global-footer/global-footer.component';
import { SharedRoutingModule } from 'src/app/library/shared/shared-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    SharedRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [GlobalHeaderComponent, GlobalFooterComponent]
})
export class SharedModule { }
