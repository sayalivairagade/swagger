import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  getProductDetail: any;

  constructor(private http: HttpClient, myRoute: Router) { }
  url=environment.url;

  addBlog(data: any): Observable<any> {
    console.log('data', data);
    return this.http.post(this.url +'addBlog', data);
  }

  editBlog(id:any,data:any):Observable<any>{
    console.log(id,data);
    return this.http.put(this.url +'editBlog?id='+ id,data);
    }

  listBlog():Observable<any>{
      return this.http.get(this.url + 'listBlog');
    }

  deleteBlog(id:any):Observable<any>{
      console.log(id);
      return this.http.delete(this.url + 'deleteBlog?id='+ id);
      }
  getBlogDetail(id:any):Observable<any>{
    console.log(id);
    return this.http.get(this.url + 'getBlogDetail?id='+ id);
    } 
}

