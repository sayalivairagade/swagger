import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentServiceService {

  constructor(private http: HttpClient, myRoute: Router) { }
  url=environment.url;
  
addPayment(data: any): Observable<any> {
    console.log('data', data);
    return this.http.post(this.url + 'addPayment', data);
}
addSummary(id:any,data: any): Observable<any> {
  console.log('data', data);
  return this.http.post(this.url + 'addSummary?id='+ id , data);
}
deletecartdetails(id: any): Observable<any> {
  console.log(id);
  return this.http.delete(this.url + 'deleteallcart?id=' + id);
}
listSummary(id:any):Observable<any>{
  return this.http.get(this.url + 'listSummary?id=' + id);
}
}
