import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogDetailService {

  constructor(private http: HttpClient, myRoute: Router) { }
  url=environment.url;

  getBlogDetail(id:any):Observable<any>{
    console.log(id);
    return this.http.get(this.url+'listComment?id='+ id);
    } 
    addComment(data: any): Observable<any> {
      console.log('data', data);
      return this.http.post(this.url+'addComment', data);
    }
    deleteComment(id:any):Observable<any>{
      console.log(id);
      return this.http.delete(this.url+'deleteComment?id='+ id);
      }
  
}
