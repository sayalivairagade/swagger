import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthRouting } from 'src/app/library/auth/auth.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './signup/signup.component';


@NgModule({
  imports: [
    CommonModule,
    AuthRouting,
    ReactiveFormsModule,
  ],
  declarations: [LoginComponent, SignupComponent]
})
export class AuthModule { }
