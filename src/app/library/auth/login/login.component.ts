import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { AuthService } from "src/app/library/service/auth.service";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;
    constructor(private formBuilder: FormBuilder,
        private auth: AuthService,
        private router: Router,
        private toastr: ToastrService) { }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
    }
    get f() { return this.loginForm.controls; }
    onsubmit() {
         
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            

            return;
        }
        this.auth.login(this.loginForm.value).subscribe((res: any) => {
            console.log('response ::', res);
            if(res.code == 500){
                this.toastr.error(res.message); 
            }
            else{
            var token = res.token; 
            localStorage.setItem("token",token);
            console.log("token created");
            var data=res.data;   
            localStorage.setItem("userid", data._id);

            localStorage.setItem("role", data.role);
            localStorage.setItem('fullname',data.fullname);
            // console.log("role define");
            if(localStorage.getItem("token")!= null && localStorage.getItem("role")=="admin"){
            this.router.navigate(["/dash-view/admin"]);
            this.toastr.success("admin login successfully");

            }
            else{
                this.router.navigate(["/users/user-dashboard"]);
                this.toastr.success("user login successfully");

            }
        }
        });

    }
    

}
