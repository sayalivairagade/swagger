import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from "src/app/library/service/auth.service";
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  myform: FormGroup;
  submitted = false;


  constructor(private formBuilder: FormBuilder,
    private auth: AuthService ,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
      this.myform = new FormGroup({
      'fullname': new FormControl('', [Validators.required ,Validators.pattern("[a-zA-Z ]*$")]),
      'email': new FormControl('', [Validators.required,Validators.email]),
      'password': new FormControl('', [Validators.required ,Validators.minLength(6)]),
    });
  }
  get f() { return this.myform.controls; }

  onsubmit() {
    this.submitted = true;

        // stop here if form is invalid
        if (this.myform.invalid) {
            return;
        }
    this.auth.signup(this.myform.value).subscribe((res: any) => {
      if(res.code == 200){
      res = res.data;
      console.log('response ::', res);
      this.router.navigate(['auth/login']);
      this.toastr.success("user login successfully");
      }
      else{
        this.toastr.error(res.message);

      }
    });

  }
}
