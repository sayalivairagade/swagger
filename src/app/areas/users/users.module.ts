import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRouting } from 'src/app/areas/users/users.routing';

@NgModule({
  imports: [
    CommonModule,
    UsersRouting,
  ],
  declarations: []
})
export class UsersModule { }
