import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRouting } from 'src/app/app.routing';
import { HomeComponent } from 'src/app/library/shared/component/home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
 import { ToastrModule } from 'ngx-toastr';
import { InterceptorServiceService } from './library/interceptor-service/interceptor-service.service';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,

  ],
  imports: [
    BrowserModule,
    AppRouting,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    FormsModule,
    NgxSpinnerModule      // SpinnerModule added
  ],
  providers: [{
   provide: HTTP_INTERCEPTORS,
   useClass: InterceptorServiceService,
  multi: true,
 }], // in providers injecting the interceptor
  bootstrap: [AppComponent] // in main.ts bootstrap module is imported
})
export class AppModule { }
