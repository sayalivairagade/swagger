import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './library/service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SecurityGuard implements CanActivate {
  constructor(private auth: AuthService,private myRoute: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      if(this.auth.isLoggednIn() && localStorage.getItem("role")=="admin"){
        this.myRoute.navigate(["/dash-view/admin"]);
        return false;
  
      }
      else if(this.auth.isLoggednIn() && localStorage.getItem("role")=="user"){
        this.myRoute.navigate(["/users/user-dashboard"]);
        return false;

      } 
      else{
  
        return true;
  
      }
  }
}
