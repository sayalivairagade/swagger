import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDashboardComponent } from './component/user-dashboard/user-dashboard.component';
import { UserViewComponent } from './user-view.component';
import { ShirtViewComponent } from './component/shirt-view/shirt-view.component';
import { JeansViewComponent } from './component/jeans-view/jeans-view.component';
import { BlogsComponent } from './component/blogs/blogs.component';
import { BlogDetailComponent } from './component/blog-detail/blog-detail.component';
import { ShirtViewDetailComponent } from './component/shirt-view-detail/shirt-view-detail.component';
import { JeansViewDetailComponent } from './component/jeans-view-detail/jeans-view-detail.component';
import { AddToCartComponent } from './component/add-to-cart/add-to-cart.component';
import { ShippingDetailComponent } from './component/shipping-detail/shipping-detail.component';
import { PaymentComponent } from './component/payment/payment.component';
import { SuccessDetailComponent } from './component/success-detail/success-detail.component';
import { OldPasswordComponent } from './component/old-password/old-password.component';
import { NewPasswordComponent } from './component/new-password/new-password.component';
import { EditProfileComponent } from './component/edit-profile/edit-profile.component';

const routes: Routes = [
  {
    path:"",
    component:UserViewComponent,children:[

    {
      path:"user-dashboard",
      component:UserDashboardComponent
    },
    {
      path:"shirts",
      component:ShirtViewComponent
    },
    {
      path:"shirt-view-detail/:id",
      component:ShirtViewDetailComponent
    },
    {
      path:"jeans",
      component:JeansViewComponent
    },
    {
      path:"jeans-view-detail/:id",
      component:JeansViewDetailComponent
    },
    {
      path:"blogs",
      component:BlogsComponent
    },
    {
      path:"blog-view/:id",
      component:BlogDetailComponent
    },
    {
      path:"cartlist",
      component:AddToCartComponent
    },
    {
      path:"shipping-detail",
      component:ShippingDetailComponent
    },
    {
      path:"pay/:id",
      component:PaymentComponent
    },
    {
      path:"payment/:id",
      component:SuccessDetailComponent
    },
    {
      path:"old-password/:id",
      component:OldPasswordComponent
    },
    {
      path:"new-password/:id",
      component:NewPasswordComponent
    },
    {
      path:"edit-profile/:id",
      component:EditProfileComponent
    }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserViewRoutingModule { }
