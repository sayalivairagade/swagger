import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserViewRoutingModule } from './user-view-routing.module';
import { UserDashboardComponent } from './component/user-dashboard/user-dashboard.component';
import { HeaderComponent } from './nav/header/header.component';
import { SidebarComponent } from './nav/sidebar/sidebar.component';
import { UserViewComponent } from './user-view.component';
import { ShirtViewComponent } from './component/shirt-view/shirt-view.component';
import { JeansViewComponent } from './component/jeans-view/jeans-view.component';
import { BlogsComponent } from './component/blogs/blogs.component';
import { BlogDetailComponent } from './component/blog-detail/blog-detail.component';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {RatingModule} from 'primeng/rating';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ShirtViewDetailComponent } from './component/shirt-view-detail/shirt-view-detail.component';
import { JeansViewDetailComponent } from './component/jeans-view-detail/jeans-view-detail.component';
import { AddToCartComponent } from './component/add-to-cart/add-to-cart.component';
import { ShippingDetailComponent } from './component/shipping-detail/shipping-detail.component';
import {SpinnerModule} from 'primeng/spinner';
import { PaymentComponent } from './component/payment/payment.component';
import { SuccessDetailComponent } from './component/success-detail/success-detail.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import {TableModule} from 'primeng/table';
import { OldPasswordComponent } from './component/old-password/old-password.component';
import { NewPasswordComponent } from './component/new-password/new-password.component';
import { EditProfileComponent } from './component/edit-profile/edit-profile.component';

@NgModule({
  imports: [
    CommonModule,
    UserViewRoutingModule,
    ScrollPanelModule,
    RatingModule,
    FormsModule,
    ReactiveFormsModule,
    SpinnerModule,
    NgxSpinnerModule,
    TableModule
  ],
  declarations: [UserDashboardComponent, HeaderComponent, SidebarComponent, UserViewComponent, ShirtViewComponent, JeansViewComponent, BlogsComponent, BlogDetailComponent, ShirtViewDetailComponent, JeansViewDetailComponent, AddToCartComponent, ShippingDetailComponent, PaymentComponent, SuccessDetailComponent, OldPasswordComponent, NewPasswordComponent, EditProfileComponent]
})
export class UserViewModule { }
