import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../../../library/product-service/product-service.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-jeans-view',
  templateUrl: './jeans-view.component.html',
  styleUrls: ['./jeans-view.component.css']
})
export class JeansViewComponent implements OnInit {
  products: any;

  constructor(private auth:ProductServiceService,
  private router:Router,
private toastr:ToastrService) { }

  ngOnInit() {
    this.getJeans();

  }
  getJeans() {
    this.auth.listJeans().subscribe(res => {
      if(res.code == 200){
      this.products = res.data;
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.error("Something went wrong ");

      }
    });
  }
}
