import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JeansViewComponent } from './jeans-view.component';

describe('JeansViewComponent', () => {
  let component: JeansViewComponent;
  let fixture: ComponentFixture<JeansViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JeansViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JeansViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
