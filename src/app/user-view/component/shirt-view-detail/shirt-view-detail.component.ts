import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductServiceService } from '../../../library/product-service/product-service.service';
import { AddtocartServiceService } from '../../../library/addtocart-service/addtocart-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shirt-view-detail',
  templateUrl: './shirt-view-detail.component.html',
  styleUrls: ['./shirt-view-detail.component.css']
})
export class ShirtViewDetailComponent implements OnInit {
  userData: any;

  constructor(private route:ActivatedRoute, 
    private cart:AddtocartServiceService ,
    private auth:ProductServiceService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.route.params.subscribe(params =>{
      console.log(params)
      this.auth.getProductDetail(params.id).subscribe(res =>{
        console.log(res)
        this.userData = res.data;
      })
    })

  }

  onSubmit(){
   
      var data={
        "user_id":localStorage.getItem("userid"),
        "product_id":this.userData._id,
        "productPrice": this.userData.productcost,
      }
      this.cart.addCart(data).subscribe(res=>{
        if(res.code ==200){
          // window.location.reload();
          this.toastr.success("Product Added Successfully");
        }
      })


  }

}
