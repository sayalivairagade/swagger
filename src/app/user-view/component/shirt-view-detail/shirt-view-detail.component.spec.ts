import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirtViewDetailComponent } from './shirt-view-detail.component';

describe('ShirtViewDetailComponent', () => {
  let component: ShirtViewDetailComponent;
  let fixture: ComponentFixture<ShirtViewDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirtViewDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirtViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
