import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../library/blog-service/blog.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.css']
})
export class BlogsComponent implements OnInit {
  products: any;

  constructor(private auth: BlogService,
  private router:Router,
private toastr:ToastrService) { }

  ngOnInit() {
    this.getProduct();

  }
  getProduct() {
    this.auth.listBlog().subscribe(res => {
      if(res.code == 200){
      this.products = res.data;
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([""]);
      }
      else{
        this.toastr.error("Something went wrong ");

      }
    });
  }

}
