import { Component, OnInit } from '@angular/core';
import { AfterViewInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AddtocartServiceService } from '../../../library/addtocart-service/addtocart-service.service';
import { PaymentServiceService } from '../../../library/payment-service/payment-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements AfterViewInit, OnDestroy {
  @ViewChild('cardInfo') cardInfo: ElementRef;

  data: any;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  amount: any;

  constructor(private cd: ChangeDetectorRef,
    private router: Router,
    private cart: AddtocartServiceService,
    private pay: PaymentServiceService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.getcart();
  }
  getcart() {

    var userid = localStorage.getItem("userid")
    this.cart.listCart(userid).subscribe(res => {
      if(res.code == 200){
      this.amount = res.totalprice;
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.error("Something went wrong ");

      }
    });
  }
  ngAfterViewInit() {
    const style = {
      base: {
        lineHeight: '24px',
        fontFamily: 'monospace',
        fontSmoothing: 'antialiased',
        fontSize: '19px',
        '::placeholder': {
          color: 'purple'
        }
      }
    };

    this.card = elements.create('card', { style });
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }


  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);
    if (error) {
      console.log('Something is wrong:', error);
    }
    else {
      // ...send the token to the your backend to process the charge
      this.data = {
        "userid": localStorage.getItem("userid"),
        "amount": this.amount,
        "token": token.id
      }
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide();
      }, 5000);
      this.pay.addPayment(this.data).subscribe(res => {
        if (res.code == 200) {
          var uid = localStorage.getItem("userid");
          var data1 = {
            'userid': localStorage.getItem('userid'),
            'total_amount': this.amount,
            'transaction_id': res.data.id,
          }
          this.route.params.subscribe(params => {
            this.pay.addSummary(params.id, data1).subscribe(res => {
              if(res.code == 200){
              this.toastr.success("Payment Successful");
              this.router.navigate(['sucessfull/payment', uid]);
              }
              else if (res.code==401){
                alert("Session Expired.Please login");
                this.router.navigate([''])
              }
              else{
                this.toastr.error("Something went wrong ");
        
              }      
            })
          })
        }
      })
    }
  }
}
