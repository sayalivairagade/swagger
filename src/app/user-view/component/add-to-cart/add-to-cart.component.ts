import { Component, OnInit } from '@angular/core';
import { AddtocartServiceService } from '../../../library/addtocart-service/addtocart-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrls: ['./add-to-cart.component.css']
})
export class AddToCartComponent implements OnInit {
  userData: any;
  value: any;
  val: any;
  amount: number;
  buttonStatus = true;
  empty: string;

  constructor(private cart: AddtocartServiceService,
    private toastr: ToastrService,
  private router:Router) { }

  ngOnInit() {


    this.getcart();

  }
  getcart() {
    var userid = localStorage.getItem("userid")
    this.cart.listCart(userid).subscribe(res => {
      if(res.code == 200){
      this.userData = res.data;
      this.amount = res.totalprice;
      if (this.userData != 0) {
        this.buttonStatus = false;
      }
      else {
        this.buttonStatus = true;
        this.empty = "Your cart is empty";
      }
    }
    else if (res.code==401){
      alert("Session Expired.Please login");
      this.router.navigate([''])
    }
    else{
      this.toastr.success("Something went wrong ");

    }

    });
  }

  updatecart(value, i) {
    console.log('index', i, ' value', value)
    this.userData.map((data, index) => {
      if (i == index) {
        data.productPrice = data.product_id.productcost * value.quantity
        this.amount += data.product_id.productcost;
      }

    })
    var data = {
      "user_id": localStorage.getItem("userid"),
      "product_id": value.product_id._id,
      "quantity": value.quantity,
      "productPrice": value.product_id.productcost,
    }
    this.cart.updateCart(data).subscribe((res: any) => {
      if (res.code == 200){
        this.toastr.success("Update Succesfully")
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.error("Something went wrong ");

      }

      this.getcart()
    });

  }
  deleteCart(id) {
    this.cart.deleteFromCart(id).subscribe(res => {
      if (res.code == 200){
      this.toastr.success("Product Deleted Successfully");
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }
      this.getcart();
    });
  }

}
