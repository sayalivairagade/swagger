import { Component, OnInit } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { FormControl } from "@angular/forms";
import { Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/library/service/auth.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {
  newPassform: FormGroup;
  submitted = false;
  constructor(private route: ActivatedRoute,
    private auth: AuthService,
    private toastr: ToastrService,
  private router:Router) { }

  ngOnInit() {
    this.newPassform = new FormGroup({
      'password': new FormControl('', [Validators.required]),
      'confirmpassword': new FormControl('', [Validators.required]),    
    });

  }
  get f() { return this.newPassform.controls; }

  onsubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.newPassform.invalid) {
      return;
    }
if(this.newPassform.value.password != this.newPassform.value.confirmpassword){
  this.toastr.error("Not Matched");
}
else{
    this.route.params.subscribe(params => {
      this.auth.newPasswordChange(params.id, this.newPassform.value).subscribe(res => {
        if (res.code == 200) {
          this.toastr.success("Password Reset Successfully");

        }
        else if (res.code==401){
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else{
          this.toastr.error("Something went wrong ");
  
        }

      })
    })
  }
  }

}
