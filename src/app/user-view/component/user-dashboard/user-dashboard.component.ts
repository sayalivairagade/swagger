import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit {
  name: string;

  constructor() { }

  ngOnInit() {
    this.name = localStorage.getItem("fullname");

    
  }

}
