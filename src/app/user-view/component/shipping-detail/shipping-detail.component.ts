import { Component, OnInit } from '@angular/core';
import { ShippingServiceService } from '../../../library/shipping-service/shipping-service.service';
import { ToastrService } from 'ngx-toastr';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-shipping-detail',
  templateUrl: './shipping-detail.component.html',
  styleUrls: ['./shipping-detail.component.css']
})
export class ShippingDetailComponent implements OnInit {
  myform: FormGroup;
  submitted = false;
  buttonStatus = false;

  constructor(private ship:ShippingServiceService,
    private router: Router,
    private toastr: ToastrService,private spinner: NgxSpinnerService) { }
  ngOnInit() {
      this.myform = new FormGroup({
        'firstname': new FormControl('', [Validators.required,Validators.pattern("[a-zA-Z ]*$")]),
        'lastname': new FormControl('', [Validators.required,Validators.pattern("[a-zA-Z ]*$")]),
        'email': new FormControl('', [Validators.required ,Validators.email]),
        'contact': new FormControl('', [Validators.required ,Validators.minLength(10)]),
        'address_line_1': new FormControl('', [Validators.required]),
        'address_line_2': new FormControl('', [Validators.required]),
        'state': new FormControl('', [Validators.required]),
        'country': new FormControl('', [Validators.required]),
        'city': new FormControl('', [Validators.required]),
        'zipcode': new FormControl('', [Validators.required ,Validators.minLength(6)]),
  
      });
}
    
  get f() { return this.myform.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    
    if (this.myform.invalid) {
        return;
    }
    this.spinner.show();
    setTimeout(() => {   
      this.spinner.hide();
  }, 5000);
      this.myform.value.user_id = localStorage.getItem("userid");
      this.ship.shippingDetail(this.myform.value).subscribe((res: any) => {
        if(res.code == 200){
        res = res.data;
        this.toastr.success(" successfully done");
        var userid = localStorage.getItem("userid")
        this.router.navigate(['/payment/pay',userid]);
        }
        else if (res.code==401){
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else{
          this.toastr.error("Something went wrong ");
  
        }
      });
   }
  
}
