import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessDetailComponent } from './success-detail.component';

describe('SuccessDetailComponent', () => {
  let component: SuccessDetailComponent;
  let fixture: ComponentFixture<SuccessDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
