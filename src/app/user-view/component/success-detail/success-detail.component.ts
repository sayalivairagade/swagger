import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PaymentServiceService } from '../../../library/payment-service/payment-service.service';

@Component({
  selector: 'app-success-detail',
  templateUrl: './success-detail.component.html',
  styleUrls: ['./success-detail.component.css']
})
export class SuccessDetailComponent implements OnInit {
  finalSummary: any;
  finalShippingList: any;
  finalCartList: any;
  constructor(private route:ActivatedRoute , 
              private success:PaymentServiceService) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.success.listSummary(params.id).subscribe(res => {
        this.finalSummary = res.data;
        this.finalCartList = res.data.finalCart
        this.finalShippingList = res.data.finalShipping
        console.log("data", this.finalCartList)

      })
    })
    this.route.params.subscribe(params => {
      this.success.deletecartdetails(params.id).subscribe(res => {

      })
    })


  }

}
  
