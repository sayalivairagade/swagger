import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../../library/service/auth.service';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  myform: FormGroup;
  userData: any;
  submitted = false;
  
  constructor(private auth:AuthService,
    private route:ActivatedRoute,
    private router:Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.myform = new FormGroup({
      'fullname': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required]),
    });
     
    this.route.params.subscribe(params =>{
        this.auth.getUserDetail(params.id).subscribe(res =>{
          this.userData = res.data;
          this.myform.patchValue({
            fullname:this.userData.fullname,
            email:this.userData.email,
          })
        })
  });
  }
  onsubmit() {
    this.route.params.subscribe(params =>{
      this.auth.editUserProfile(params.id,this.myform.value).subscribe(res =>{
        if (res.code == 200){
          this.toastr.success("User Edit Successfully")

        }
        else if (res.code==401){
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else{
          this.toastr.error("Something went wrong ");
  
        }
     });
   });  
  }
}
