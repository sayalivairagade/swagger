import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { AuthService } from "src/app/library/service/auth.service";
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-old-password',
  templateUrl: './old-password.component.html',
  styleUrls: ['./old-password.component.css']
})
export class OldPasswordComponent implements OnInit {
  oldPasswordForm: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.oldPasswordForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  get f() { return this.oldPasswordForm.controls; }
  onsubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.oldPasswordForm.invalid) {
      return;
    }

    this.route.params.subscribe(params => {
      this.auth.oldPassword(params.id, this.oldPasswordForm.value).subscribe(res => {
        if (res.code == 200) {
          this.toastr.success("successfully done");
          var id = localStorage.getItem("userid");
          this.router.navigate(["users/new-password", id]);
        }
        else if (res.code == 401) {
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else {
          this.toastr.error("Something went wrong ");
        }
      })
    })
  }
}

