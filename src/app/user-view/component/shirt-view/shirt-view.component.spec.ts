import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirtViewComponent } from './shirt-view.component';

describe('ShirtViewComponent', () => {
  let component: ShirtViewComponent;
  let fixture: ComponentFixture<ShirtViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShirtViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShirtViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
