import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JeansViewDetailComponent } from './jeans-view-detail.component';

describe('JeansViewDetailComponent', () => {
  let component: JeansViewDetailComponent;
  let fixture: ComponentFixture<JeansViewDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JeansViewDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JeansViewDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
