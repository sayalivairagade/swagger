import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../../../library/product-service/product-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AddtocartServiceService } from '../../../library/addtocart-service/addtocart-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-jeans-view-detail',
  templateUrl: './jeans-view-detail.component.html',
  styleUrls: ['./jeans-view-detail.component.css']
})
export class JeansViewDetailComponent implements OnInit {
  userData: any;

  constructor(private route: ActivatedRoute,
     private auth: ProductServiceService, 
     private cart: AddtocartServiceService,
     private toastr: ToastrService,
    private router:Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.auth.getProductDetail(params.id).subscribe(res => {
        if(res.code == 200){
        this.userData = res.data;
        }
        else if (res.code==401){
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else{
          this.toastr.error("Something went wrong ");
  
        }
      })
    })

  }
  onSubmit() {

    var data = {
      "user_id": localStorage.getItem("userid"),
      "product_id": this.userData._id,
      "productPrice": this.userData.productcost,
    }
    this.cart.addCart(data).subscribe(res => {
      if (res.code == 200) {
        this.toastr.success("Product Added Successfully");
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.error("Something went wrong ");

      }
    })


  }

}
