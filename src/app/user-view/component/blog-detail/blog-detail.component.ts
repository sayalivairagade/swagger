import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogDetailService } from '../../../library/blog-detail-service/blog-detail.service';
import { BlogService } from '../../../library/blog-service/blog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.css']
})
export class BlogDetailComponent implements OnInit {
  commentList: any[]
  blogList = new Array
  userList = new Array
  val : number

  constructor(private route: ActivatedRoute, 
    private auth: BlogDetailService,
     private blog:BlogService,
     private toastr: ToastrService,
     private router :Router
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.blog.getBlogDetail(params.id).subscribe(res => {
        if(res.code == 200){
        this.blogList.push(res.data);
        }
        else if (res.code==401){
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else{
          this.toastr.error("Something went wrong ");
  
        }
  
      })
    })    
    this.getblogDetail()
  }

  getblogDetail() {
    this.route.params.subscribe(params => {
      this.auth.getBlogDetail(params.id).subscribe(res => {
        if(res.code == 200){
        this.commentList = res.data;
        }
        else if (res.code==401){
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else{
          this.toastr.error("Something went wrong ");
  
        }
  

    
      })

    })
  }

  onSubmit(){
    var comment=(document.getElementById("comment")as HTMLInputElement).value
    this.route.params.subscribe(params => {
      var data={
        "userid":localStorage.getItem("userid"),
        "blogid":params.id,
        "comment":comment,
         "rating":this.val
      }
      this.auth.addComment(data).subscribe(res=>{
        if(res.code ==200){
          window.location.reload();
          this.toastr.success("Thank You For Your Comment");

        }
        else if (res.code==401){
          alert("Session Expired.Please login");
          this.router.navigate([''])
        }
        else{
          this.toastr.error("Something went wrong ");
  
        }
  
      })

})
  }
}

