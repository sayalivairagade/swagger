import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { LayoutComponent } from './layout.component';
import { AddUsersComponent } from './components/users/add-users/add-users.component';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { DashViewComponent } from './components/dash-view/dash-view.component';
import { EditUsersComponent } from './components/users/edit-users/edit-users.component';
import { AddProductComponent } from './components/product_manage/add-product/add-product.component';
import { ListProductComponent } from './components/product_manage/list-product/list-product.component';
import { EditProductComponent } from './components/product_manage/edit-product/edit-product.component';
import { AddBlogComponent } from './components/manage_blogs/add-blog/add-blog.component';
import { EditBlogComponent } from './components/manage_blogs/edit-blog/edit-blog.component';
import { ListBlogComponent } from './components/manage_blogs/list-blog/list-blog.component';
import { ListCommentsComponent } from './components/comment_manage/list-comments/list-comments.component';
import { CommentDetailComponent } from './components/comment-detail/comment-detail.component';
import { ListShirtComponent } from './components/product_manage/list-shirt/list-shirt.component';

const routes: Routes = [
  {
    path:"" ,
    component:LayoutComponent,
    children:[
    {
      path:"admin",
      component:AdminComponent
    },
    {
      path:"addUser",
      component:AddUsersComponent
    },
    {
      path:"listUser",
      component:ListUsersComponent
    },
    {
      path:"editUser/:id",
      component:EditUsersComponent
    },
    {
      path:"user",
      component:DashViewComponent
    },
    {
      path:"addProduct",
      component:AddProductComponent
    },
    {
      path:"listProduct",
      component:ListProductComponent
    },
    {
      path:"editProduct/:id",
      component:EditProductComponent
    },
    {
      path:"addBlog",
      component:AddBlogComponent
    },
    {
      path:"listBlog",
      component:ListBlogComponent //EditBlogComponent
    },
    {
      path:"editBlog/:id",
      component:EditBlogComponent
    },
    {
      path:"listComments",
      component:ListCommentsComponent

    },
    {
      path:"commentDetails/:id",
      component:CommentDetailComponent
    },
    {
      path:"listShirt",
      component:ListShirtComponent
    }

    ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
