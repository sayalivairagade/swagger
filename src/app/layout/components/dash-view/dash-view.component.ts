import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dash-view',
  templateUrl: './dash-view.component.html',
  styleUrls: ['./dash-view.component.css']
})
export class DashViewComponent implements OnInit {
  constructor(private router:Router) { }

  ngOnInit() {
  }
  logout(){
    localStorage.removeItem("token");
    localStorage.removeItem("role");
    this.router.navigate([""])
}
}