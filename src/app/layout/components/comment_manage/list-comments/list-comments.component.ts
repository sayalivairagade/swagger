import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../../library/blog-service/blog.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-comments',
  templateUrl: './list-comments.component.html',
  styleUrls: ['./list-comments.component.css']
})
export class ListCommentsComponent implements OnInit {
  products: any;

  constructor(private auth: BlogService,
    private router:Router,
  private toastr:ToastrService) { }

  ngOnInit() {
    this.getProduct();

  }
  getProduct() {
    this.auth.listBlog().subscribe(res => {
      if(res.code == 200){
      this.products = res.data;
       }
       else if (res.code == 401){
         alert("Session Expired.Please login");
         this.router.navigate([''])
       }
       else{
         this.toastr.success("Something went wrong ");
       }

     });
  }
}
