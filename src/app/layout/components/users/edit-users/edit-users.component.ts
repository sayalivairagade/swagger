import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../library/service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent implements OnInit {
  myform: FormGroup;
  userData: any;
  submitted = false;


  constructor(private auth:AuthService,
    private route:ActivatedRoute,
    private toastr: ToastrService,
  private router:Router) { }

  ngOnInit() {
    this.myform = new FormGroup({
      'fullname': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required]),
      'role': new FormControl('', Validators.required)
    });
     
    this.route.params.subscribe(params =>{
        console.log(params)
        this.auth.getUserDetail(params.id).subscribe(res =>{
          console.log(res)
          this.userData = res.data;
    
          console.log(this.userData)
          this.myform.patchValue({
            fullname:this.userData.fullname,
            email:this.userData.email,
            password:this.userData.password,
            role:this.userData.role
          })
        })
  });

}   
onsubmit() {
  this.route.params.subscribe(params =>{
    this.auth.editUser(params.id,this.myform.value).subscribe(res =>{
      if(res.data == 200){
        this.toastr.success("user edit successfully");


      }
      else if (res.code == 401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }
   });
 });

}
} 