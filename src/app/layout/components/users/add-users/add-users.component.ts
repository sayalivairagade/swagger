import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../library/service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {
  myform: FormGroup;
  submitted = false;

  constructor(private auth:AuthService,
    private toastr: ToastrService,
  private router:Router) { }

  ngOnInit() {
    this.myform = new FormGroup({
      'fullname': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required ,Validators.email]),
      'password': new FormControl('', [Validators.required ,Validators.minLength(6)]),
      'role': new FormControl('', Validators.required)
    });
  }
  get f() { return this.myform.controls; }

  onsubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.myform.invalid) {
        return;
    }
    this.auth.addUser(this.myform.value).subscribe((res: any) => {
      if(res.code == 200){
       res = res.data;
       this.toastr.success("user successfully added");
      }
      else if (res.code == 401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }

     });
   }

}
