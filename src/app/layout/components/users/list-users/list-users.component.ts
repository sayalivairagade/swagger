import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../library/service/auth.service';
import { RouterState, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  products: any;

  constructor(private auth: AuthService,
  private router :Router,
  private toastr: ToastrService ) { }

  ngOnInit() {
    this.getProduct();
  }
  getProduct() {
    this.auth.listuser().subscribe(res => {
      if(res.data == 200){
      this.products = res;
      }
      else if (res.code ==401){
        alert ("Session Expired.Please login");
        this.router.navigate(['']);
      }
      else{
        this.toastr.error("Something went wrong");   
      }
    });
  }
  deleteProduct(id) {
    this.auth.deleteUser(id).subscribe(res => {
      if (res.code== 200) {
        this.toastr.success("Product Deleted Successfully");
      }else if(res.code==401){
        alert("Session Expired.Plaese Login" );
      }
      else{
        console.log("Something went wrong")
      }
      this.getProduct();
    });
  }


}
