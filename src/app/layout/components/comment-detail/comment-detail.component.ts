import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlogDetailService } from '../../../library/blog-detail-service/blog-detail.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-comment-detail',
  templateUrl: './comment-detail.component.html',
  styleUrls: ['./comment-detail.component.css']
})
export class CommentDetailComponent implements OnInit {
  commentList= new Array;
  

  constructor(private route: ActivatedRoute ,
    private auth: BlogDetailService,
    private toastr: ToastrService,
  private router:Router) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.auth.getBlogDetail(params.id).subscribe(res => {
         if (res.code == 200){
        this.commentList= res.data;
         }
        else if (res.code==401){
           alert("Session Expired.Please login");
           this.router.navigate([''])
         }
         else{
           this.toastr.success("Something went wrong ");
  
         }  
      
       })
    })
  }

  deleteComment(id) {
    console.log("...id");
    this.auth.deleteComment(id).subscribe(res => {
      if(res.data == 200){
      this.toastr.success("Product Deleted Successfully");
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }

       this.ngOnInit();
    });
  }

}
  


