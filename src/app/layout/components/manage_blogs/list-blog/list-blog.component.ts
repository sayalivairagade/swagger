import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../../library/blog-service/blog.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.css']
})
export class ListBlogComponent implements OnInit {
  products: any;

  constructor(private auth: BlogService,
    private toastr: ToastrService,
  private router:Router) { }

  ngOnInit() {
    this.getProduct();

  }
  getProduct() {
    this.auth.listBlog().subscribe(res => {
      if(res.code == 200){
      this.products = res;
      }
      else if (res.code ==401){
        alert ("Session Expired.Please login");
        this.router.navigate(['']);
      }
      else{
        this.toastr.error("Something went wrong");   
      }
    });
  }

  deleteBlog(id) {
    this.auth.deleteBlog(id).subscribe(res => {
      if(res.code == 200){
        this.toastr.success("Product Deleted Successfully");
      }else if(res.code==401){
        alert("Session Expired.Plaese Login" );
      }
      else{
        console.log("Something went wrong")
      }
      this.getProduct();
    });
  }
}
