import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BlogService } from '../../../../library/blog-service/blog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.css']
})
export class AddBlogComponent implements OnInit {
  myform: FormGroup;
  submitted = false;



  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService
    ,private auth:BlogService,
  private router:Router) { }

  
  ngOnInit() {
    this.myform = this.formBuilder.group({
      title: new FormControl('', [Validators.required]),
      blog: new FormControl('', [Validators.required]),
      file: null
    });
  
  }
  get f() { return this.myform.controls; }
  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.myform.get('file').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
    input.append('title', this.myform.get('title').value);
    input.append('blog', this.myform.get('blog').value);
    input.append('file', this.myform.get('file').value);


    return input;
  }

  onsubmit() {
    this.submitted = true;
    const formModel = this.prepareSave();

    // stop here if form is invalid
    if (this.myform.invalid) {
        return;
    }
    this.auth.addBlog(formModel).subscribe((res: any) => {
      if (res.code == 200){
       res = res.data;
        this.toastr.success("Blog successfully added");
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }

     });
    
   }
  
}
