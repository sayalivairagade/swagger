import { Component, OnInit } from '@angular/core';
import { BlogService } from '../../../../library/blog-service/blog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {
  myform: FormGroup;
  userData: any;
  submitted = false;
  url: any;


  constructor(private formBuilder: FormBuilder,
    private auth:BlogService,
    private route:ActivatedRoute,
    private toastr: ToastrService,
  private router :Router) { }

  ngOnInit() {
    this.myform = this.formBuilder.group({
      title: new FormControl('', [Validators.required]),
      blog: new FormControl('', [Validators.required]),
      file: null
    });
    this.route.params.subscribe(params =>{
      this.auth.getBlogDetail(params.id).subscribe(res =>{
        this.userData = res.data;
          this.myform.patchValue({
          title:this.userData.title,
          blog:this.userData.blog,

        })
        this.url = this.userData.image;
      })
});
}
  get f() { return this.myform.controls; }
  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.myform.get('file').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
    input.append('title', this.myform.get('title').value);
    input.append('blog', this.myform.get('blog').value);
    input.append('file', this.myform.get('file').value);

    return input;
  }

onsubmit() {
  const formModel = this.prepareSave();
  this.route.params.subscribe(params =>{
    this.auth.editBlog(params.id,formModel).subscribe(res =>{
      if (res.code == 200){
      this.toastr.success("Blog edited successfully ");
      }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }

   });
 });

}

}
