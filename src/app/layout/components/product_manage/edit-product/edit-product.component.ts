import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProductServiceService } from '../../../../library/product-service/product-service.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  myform: FormGroup;
  userData: any;
  submitted = false;
  url: any;


  constructor(private formBuilder: FormBuilder,private router:Router,private auth:ProductServiceService,private route:ActivatedRoute,private toastr: ToastrService) { }

  ngOnInit() {
    this.myform =this.formBuilder.group({
      'productname': new FormControl('', [Validators.required]),
      'productcompanyname': new FormControl('', [Validators.required]),
      'productdescription': new FormControl('', [Validators.required]),
      'productcategory': new FormControl('', Validators.required),
      'productcost': new FormControl('', Validators.required),
      file: null

    });
    this.route.params.subscribe(params =>{
      this.auth.getProductDetail(params.id).subscribe(res =>{
        this.userData = res.data;
  
        console.log(this.userData)
        this.myform.patchValue({
          productname:this.userData.productname,
          productcompanyname:this.userData.productcompanyname,
          productdescription:this.userData.productdescription,
          productcategory:this.userData.productcategory,
          productcost:this.userData.productcost,
          
        })
        this.url = this.userData.image;

      })
});
}
get f() { return this.myform.controls; }
onFileChange(event) {
  if(event.target.files.length > 0) {
    let file = event.target.files[0];
    this.myform.get('file').setValue(file);
  }
}
private prepareSave(): any {
  let input = new FormData();
  // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
  input.append('productname', this.myform.get('productname').value);
  input.append('productcompanyname', this.myform.get('productcompanyname').value);
  input.append('productdescription', this.myform.get('productdescription').value);
  input.append('productcategory', this.myform.get('productcategory').value);
  input.append('productcost', this.myform.get('productcost').value);
  input.append('file', this.myform.get('file').value);

  return input;
}
onsubmit() {
  const formModel = this.prepareSave();

  this.route.params.subscribe(params =>{
    this.auth.editProduct(params.id,formModel).subscribe(res =>{
      if(res.code ==200){
        this.toastr.success("Product edited successfully ");
  }
      else if (res.code==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }
 });
 });
}

}
