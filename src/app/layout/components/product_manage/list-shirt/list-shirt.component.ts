import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../../../../library/product-service/product-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-shirt',
  templateUrl: './list-shirt.component.html',
  styleUrls: ['./list-shirt.component.css']
})
export class ListShirtComponent implements OnInit {
  products: any;

  constructor(private auth:ProductServiceService,
    private toastr: ToastrService,
    private router:Router) { }

  ngOnInit() {
    this.getProduct();

  }
  getProduct() {
    this.auth.listProduct().subscribe(res => {
      if (res.code == 200){
        this.products = res.data;
      }
      else if (res.code ==401){
        alert ("Session Expired.Please login");
        this.router.navigate(['']);
      }
      else{
        this.toastr.error("Something went wrong");   
      }
    });
  }
  
  deleteProduct(id) {
    console.log("...id");
    this.auth.deleteProduct(id).subscribe(res => {

      if(res.code == 200){
        this.toastr.success("Product Deleted Successfully");
      }else if(res.code==401){
        alert("Session Expired.Plaese Login" );
      }
      else{
        this.toastr.error("Something went wrong")
      }
      this.getProduct();
    });
  }

}
