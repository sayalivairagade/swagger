import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ProductServiceService } from '../../../../library/product-service/product-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  myform: FormGroup;
  submitted = false;

  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(private formBuilder: FormBuilder,
    private auth:ProductServiceService,
    private toastr: ToastrService,private router:Router) { }

  ngOnInit() {
    this.myform = this.formBuilder.group({
      productname: new FormControl('', [Validators.required]),
      productcompanyname: new FormControl('', [Validators.required]),
      productdescription: new FormControl('', [Validators.required]),
      productcategory: new FormControl('', [Validators.required]),
      productcost: new FormControl('', [Validators.required]),
      file: null
    });
  }
  get f() { return this.myform.controls; }

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.myform.get('file').setValue(file);
    }
  }

  private prepareSave(): any {
    let input = new FormData();
    // This can be done a lot prettier; for example automatically assigning values by looping through `this.form.controls`, but we'll keep it as simple as possible here
    input.append('productname', this.myform.get('productname').value);
    input.append('productcompanyname', this.myform.get('productcompanyname').value);
    input.append('productdescription', this.myform.get('productdescription').value);
    input.append('productcategory', this.myform.get('productcategory').value);
    input.append('productcost', this.myform.get('productcost').value);
    input.append('file', this.myform.get('file').value);


    return input;
  }

  onsubmit() {
    this.submitted = true;
    const formModel = this.prepareSave();

    // stop here if form is invalid
    if (this.myform.invalid) {
        return;
    }
    this.auth.addProductData(formModel).subscribe((res: any) => {
      if (res.code == 200){
        this.toastr.success("Product successfully added");
      }
      else if (res.code ==401){
        alert("Session Expired.Please login");
        this.router.navigate([''])
      }
      else{
        this.toastr.success("Something went wrong ");

      }
     });
    
   }
}
