import { Component, OnInit } from '@angular/core';
import { ProductServiceService } from '../../../../library/product-service/product-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products: any;

  constructor(private auth:ProductServiceService,
    private toastr: ToastrService,
    private router:Router) { }

  ngOnInit() {
    this.getJeans();

  }
  getJeans() {
    this.auth.listJeans().subscribe(res => {
      if (res.code == 200){
        this.products = res.data;
      }
      else if (res.code ==401){
        alert ("Session Expired.Please login");
        this.router.navigate(['']);
      }
      else{
        this.toastr.error("Something went wrong");   
      }
     
    });
  }

  deleteProduct(id) {
    console.log("...id");
    this.auth.deleteProduct(id).subscribe(res => {
      if(res.code == 200){
        this.toastr.success("Product Deleted Successfully");
      }else if(res.code==401){
        alert("Session Expired.Plaese Login" );
      }
      else{
        console.log("Something went wrong")
      }
      this.getJeans();
    });
  }

}
