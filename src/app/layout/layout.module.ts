import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout-routing.module';
import { AdminComponent } from './components/admin/admin.component';
import { HeaderComponent } from './nav/header/header.component';
import { SidebarComponent } from './nav/sidebar/sidebar.component';
import { LayoutComponent } from './layout.component';
import { AddUsersComponent } from './components/users/add-users/add-users.component';
import { ListUsersComponent } from './components/users/list-users/list-users.component';
import { EditUsersComponent } from './components/users/edit-users/edit-users.component';
import { DashViewComponent } from './components/dash-view/dash-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddProductComponent } from './components/product_manage/add-product/add-product.component';
import { ListProductComponent } from './components/product_manage/list-product/list-product.component';
import { EditProductComponent } from './components/product_manage/edit-product/edit-product.component';
import { AddBlogComponent } from './components/manage_blogs/add-blog/add-blog.component';
import { ListBlogComponent } from './components/manage_blogs/list-blog/list-blog.component';
import { EditBlogComponent } from './components/manage_blogs/edit-blog/edit-blog.component';
import {EditorModule} from 'primeng/editor';
import {AccordionModule} from 'primeng/accordion';
import {ChartModule} from 'primeng/chart';
import {TableModule} from 'primeng/table';
import { ListCommentsComponent } from './components/comment_manage/list-comments/list-comments.component';
import { CommentDetailComponent } from './components/comment-detail/comment-detail.component';
import { ListShirtComponent } from './components/product_manage/list-shirt/list-shirt.component';
@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    ReactiveFormsModule,
    EditorModule,
    AccordionModule,
    ChartModule,
    TableModule

  ],
  declarations: [DashViewComponent, AdminComponent, HeaderComponent, SidebarComponent, LayoutComponent, AddUsersComponent, ListUsersComponent, EditUsersComponent, AddProductComponent, ListProductComponent, EditProductComponent, AddBlogComponent, ListBlogComponent, EditBlogComponent, ListCommentsComponent, CommentDetailComponent, ListShirtComponent]
})
export class LayoutModule { }
